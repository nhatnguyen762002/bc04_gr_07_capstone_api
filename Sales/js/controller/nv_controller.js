function renderDSSP(spArr) {
  var tbodyEl = document.querySelector("#tableDanhSach");
  tbodyEl.innerHTML = ``;

  spArr.forEach(function (sp) {
    let { id, name, price, img, desc } = sp;

    tbodyEl.innerHTML += `<tr>
        <td class="align-middle">${id}</td>
        <td class="align-middle">${name}</td>
        <td class="align-middle">${Intl.NumberFormat("vi-VN", {
          style: "currency",
          currency: "VND",
        }).format(price)}</td>
        <td class="align-middle"><img class="my-img" src="${img}"></img></td>
        <td class="align-middle">${desc}</td>
        <td class="align-middle">
          <button onclick="xoaSP('${id}')" class="btn btn-danger my-1">Xóa</button>
          <button onclick="suaSP('${id}')" data-toggle="modal" data-target="#myModal" class="btn btn-warning my-1">Sửa</button>
        </td>
      </tr>`;
  });
}

function layThongTinTuForm() {
  const idValue = document.querySelector("#id").value;
  const nameValue = document.querySelector("#name").value;
  const priceValue = document.querySelector("#price").value;
  const screenValue = document.querySelector("#screen").value;
  const backCameraValue = document.querySelector("#backCamera").value;
  const frontCameraValue = document.querySelector("#frontCamera").value;
  const imgValue = document.querySelector("#img").value;
  const descValue = document.querySelector("#desc").value;
  const typeValue = document.querySelector("#type").value;

  return new SanPham(
    nameValue,
    priceValue,
    screenValue,
    backCameraValue,
    frontCameraValue,
    imgValue,
    descValue,
    typeValue,
    idValue
  );
}

function showThongTinLenForm(sp) {
  let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
    sp;

  document.querySelector("#id").value = id;
  document.querySelector("#name").value = name;
  document.querySelector("#price").value = price;
  document.querySelector("#screen").value = screen;
  document.querySelector("#backCamera").value = backCamera;
  document.querySelector("#frontCamera").value = frontCamera;
  document.querySelector("#img").value = img;
  document.querySelector("#desc").value = desc;
  document.querySelector("#type").value = type;

  // document.querySelector("#tknv").disabled = true;
}

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}

function tatLoading() {
  document.getElementById("loading").style.display = "none";
}

function resetForm() {
  document.querySelector("#id").value = ``;
  document.querySelector("#name").value = ``;
  document.querySelector("#price").value = ``;
  document.querySelector("#screen").value = ``;
  document.querySelector("#backCamera").value = ``;
  document.querySelector("#frontCamera").value = ``;
  document.querySelector("#img").value = ``;
  document.querySelector("#desc").value = ``;

  document.querySelector("#tbMa").innerHTML = ``;
  document.querySelector("#tbTen").innerHTML = ``;
  document.querySelector("#tbGia").innerHTML = ``;
  document.querySelector("#tbManHinh").innerHTML = ``;
  document.querySelector("#tbCameraSau").innerHTML = ``;
  document.querySelector("#tbCameraTruoc").innerHTML = ``;
  document.querySelector("#tbHinhAnh").innerHTML = ``;
  document.querySelector("#tbMoTa").innerHTML = ``;

  document.querySelector("#btnThemNV").disabled = false;
  document.querySelector("#btnCapNhat").disabled = true;
}

function kiemTraThongTin(sp) {
  let isValid = validator.kiemTraRong(sp.name, "tbTen", "Tên sản phẩm");

  isValid =
    isValid &
    (validator.kiemTraRong(sp.price, "tbGia", "Giá sản phẩm") &&
      validator.kiemTraNumber(sp.price, "tbGia", "Giá chỉ được nhập số!"));

  isValid =
    isValid &
    validator.kiemTraRong(sp.screen, "tbManHinh", "Thông số màn hình");

  isValid =
    isValid &
    validator.kiemTraRong(sp.backCamera, "tbCameraSau", "Thông số camera sau");

  isValid =
    isValid &
    validator.kiemTraRong(
      sp.frontCamera,
      "tbCameraTruoc",
      "Thông số camera trước"
    );

  isValid =
    isValid & validator.kiemTraRong(sp.img, "tbHinhAnh", "Link hình ảnh");

  isValid =
    isValid & validator.kiemTraRong(sp.desc, "tbMoTa", "Thông tin mô tả");

  return isValid;
}

closeModal = () => {
  document.getElementById("btnDong").click();
};
