const BASE_URL = "https://62db6cd9e56f6d82a77287f5.mockapi.io";

document.getElementById("btnThem").addEventListener("click", () => {
  resetForm();
});

function getDSSP() {
  batLoading();
  axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  })
    .then(function (res) {
      renderDSSP(res.data);
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}

getDSSP();

function xoaSP(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/products/${id}`,
    method: "DELETE",
  })
    .then(function () {
      getDSSP();
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}

function themSV() {
  let newSP = layThongTinTuForm();
  let isValid = kiemTraThongTin(newSP);
  if (isValid) {
    batLoading();

    axios({
      url: `${BASE_URL}/products`,
      method: "POST",
      data: newSP,
    })
      .then(function (res) {
        closeModal();
        getDSSP();
        tatLoading();
        console.log("res: ", res);
      })
      .catch(function (err) {
        tatLoading();
        console.log("err: ", err);
      });
  }
}
document.getElementById("btnThemNV").addEventListener("click", function () {
  themSV();
});

function suaSP(id) {
  resetForm();
  batLoading();

  axios({
    url: `${BASE_URL}/products/${id}`,
    method: "GET",
  })
    .then(function (res) {
      showThongTinLenForm(res.data);
      document.querySelector("#btnThemNV").disabled = true;
      document.querySelector("#btnCapNhat").disabled = false;
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}

function updateSP() {
  let newSP = layThongTinTuForm();
  let isValid = kiemTraThongTin(newSP);
  if (isValid) {
    batLoading();

    axios({
      url: `${BASE_URL}/products/${newSP.id}`,
      method: "PUT",
      data: newSP,
    })
      .then(function () {
        closeModal();
        getDSSP();
        tatLoading();
      })
      .catch(function (err) {
        tatLoading();
        console.log("err: ", err);
      });
  }
}
document.getElementById("btnCapNhat").addEventListener("click", function () {
  updateSP();
});
