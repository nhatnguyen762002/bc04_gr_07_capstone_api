let validator = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(
        idError
      ).innerText = `${message} không được để rỗng!`;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraNumber: function (value, idError, message) {
    var reg = /^\d+$/;

    if (reg.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
};
